# Eternal Goth Development Toolkit
This is our internal development toolkit, it's used to automate many tasks which may
otherwise take a fair bit of time to manually perform each task.

This allows us to work more efficiently and have a useful toolkit at our finger tips.

## Fresh Rewrite
This project has recently been rewritten using pure BASH as opposed to
lua to help reduce dependencies and significantly clean up the code
which makes it far more maintainable.

You may still find the old lua based version of this toolkit in the "legacy-lua"
branch of this repository.

## System Requirements
 * GNU/Linux based operating system
 * lessc v3.12 or later
 * Rsync version 3.2 or later and its dependencies

## Included Third Party Applications
We include the following applications to be able to automate our tasks
 * Google Closure

## License
This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 3, as published by the
Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
