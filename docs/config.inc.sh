#!/bin/bash
#
# Eternal Goth Toolkit Config
#

# Core Configuration
language_used="en_GB.utf8"
configured=false                 # set to true once you've fully configured this file

# Interaction Configuration
if [ $1 ]; then
    if [ $1 = "web" ]; then
        # Configuration for website
        #
        #  Host Configuration
        DevelopmentHost=""       # Your development host
        LiveHost=""              # Your live host
        EnableExcludes=true      # Enable excluding files from upload?
        AutoFixPermissions=false # Automatically try to login via ssh and fix permissions?

        #  File Configuration
        ExcludesLive="excludes-live"
        ExcludesDev="excludes-dev"

        #  Directory Configuration
        LocalDirectory=""        # Path to your website sources
        RemoteDirectory=""       # Path to your remote web directory, example /home/user/public_html/
        JavaScriptDir=""         # Location of your javascript files
        CSSDirectory=""          # Location of your CSS files
        ImageDirectory=""        # Your image directory
        RobotsDirectory=""       # Path for your robots.txt and browserconfig.xml (Leave blank if not not outside localdir)
        RobotsStaticDirectory="" # Path for your static robots.txt and browserconfig.xml (Leave blank if not not outside localdir)
        HasStaticDirectory=false # If you don't use a static domain, disable it here.

        #  JavaScript Configuration
        GlobalScripts=("script1" "script2" "script3") # Scripts which need to be compiled, in array format

        #  Gothica CSS Configuration
        GothicaLessCSS=("css1" "css2" "css3") # CSS files which need to be built and minified using lessc, in array format
    elif [ $1 = "help" ]; then
        # Configuration for help centre.. same format as above.
        #
        #  Host Configuration
        DevelopmentHost=""
        LiveHost=""
        EnableExcludes=true
        AutoFixPermissions=true

        #  File Configuration
        ExcludesLive="excludes-live"
        ExcludesDev="excludes-dev"

        #  Directory Configuration
        LocalDirectory=""
        RemoteDirectory=""
        JavaScriptDir=""
        CSSDirectory=""
        RobotsDirectory=""
        RobotsStaticDirectory=""
        HasStaticDirectory=false

        #  JavaScript Configuration
        GlobalScripts=()

        #  Gothica CSS Configuration
        GothicaLessCSS=()
    fi
fi
