#!/bin/bash
#
#                         Eternal Goth Development Toolkit
#
# This is our internal development toolkit, it's used to automate many tasks which may
# otherwise take a fair bit of time to manually perform each task.
#
# This allows us to work more efficiently and have a useful toolkit at our finger tips.
#
# This toolkit will most likely be improved or altered as needed over time.
#
# Usage: ./gothtoolkit [OPTION...]
#  Flags:
#    (none)                           Run Toolkit
#    -c [FILE]                        Use a non-default configuration file
#    -h                               Print this help message"
#
# This program is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License version 3, as published by the Free Software
# Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see http://www.gnu.org/licenses/.
#

version="2.0.0-dev"
author="Jake Shipton"
license="GPLv3"

# Load configuration file
configFile="config/config.sh"

while getopts 'c:h' flag; do
    case "${flag}" in
        c)
            configFile="${OPTARG}"
            hasFlag=true
        ;;
        h)
            echo "                        Eternal Goth Development Toolkit"
            echo
            echo "This is our internal development toolkit, it's used to automate many tasks which may"
            echo "otherwise take a fair bit of time to manually perform each task."
            echo
            echo "This allows us to work more efficiently and have a useful toolkit at our finger tips."
            echo
            echo "This toolkit will most likely be improved or altered as needed over time."
            echo
            echo "Version: ${version}"
            echo
            echo "Usage: ${0} [FLAG..]"
            echo " Flags:"
            echo "   (none)                           Run Toolkit"
            echo "   -c [FILE]                        Use a non-default configuration file"
            echo "   -h                               Print this help message"
            echo
            echo "This program is free software; you can redistribute it and/or modify it under the"
            echo "terms of the GNU General Public License version 3, as published by the Free Software"
            echo "Foundation."
            echo
            echo "This program is distributed in the hope that it will be useful, but WITHOUT ANY"
            echo "WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR"
            echo "A PARTICULAR PURPOSE. See the GNU General Public License for more details."
            echo
            echo "You should have received a copy of the GNU General Public License along with"
            echo "this program. If not, see http://www.gnu.org/licenses/."
            exit
        ;;
	esac
done

if test -f "$configFile"; then
	source $configFile
else
	echo "ABORT! Missing config file!"
	exit 1
fi

# Setup environment
LANG=${language_used}
LANGUAGE=${language_used}

# Core functions
cleanFiles() {
    local robotFile="${LocalDirectory}${RobotsDirectory}robots.txt"
    local robotStaticFile="${LocalDirectory}${RobotsStaticDirectory}robots.txt"
    local bconfigFile="${LocalDirectory}${RobotsDirectory}browserconfig.xml"

    echo "Cleaning files..."

    if test -f "${robotFile}"; then
        rm "${robotFile}"
    else
        echo ${robotFile}
    fi

    if test -f "${robotStaticFile}"; then
        rm "${robotStaticFile}"
    fi

    echo " --> Removed robots.txt" # Maybe?

    if test -f "${bconfigFile}"; then
        rm "${bconfigFile}"
    fi

    echo " --> Removed browserconfig.xml" # Again, maybe?
}

configFiles() {
    if [ $1 ]; then
        if [ $1 == "1" ]; then
            type="dev"
        elif [ $1 == "2" ]; then
            type="live"
        else
            echo "Invalid type!"
            exit 1
        fi
    else
        echo "No type specified!"
        exit 1
    fi

    local robotInputFile="${LocalDirectory}${RobotsDirectory}${type}_robots.txt"
    local robotOutputFile="${LocalDirectory}${RobotsDirectory}robots.txt"

    if [ ${HasStaticDirectory} == true ]; then
        local robotStaticInputFile="${LocalDirectory}${RobotsStaticDirectory}${type}_robots_static.txt"
        local robotStaticOutputFile="${LocalDirectory}${RobotsStaticDirectory}robots.txt"
    fi

    local bconfigInputFile="${LocalDirectory}${RobotsDirectory}${type}_browserconfig.xml"
    local bconfigOutputFile="${LocalDirectory}${RobotsDirectory}browserconfig.xml"

    echo "Preparing files..."

    echo " --> Installing robots.txt..."
    if test -f "${robotInputFile}"; then
        cp "${robotInputFile}" "${robotOutputFile}"
    else
        echo -e " -->\033[0;31m !! Missing file: ${robotInputFile}\033[0m"
    fi

    if [ ${HasStaticDirectory} == true ]; then
        if test -f "${robotStaticInputFile}"; then
            cp "${robotStaticInputFile}" "${robotStaticOutputFile}"
        else
            echo -e " -->\033[0;31m !! Missing file: ${robotStaticInputFile}\033[0m"
        fi
    fi

    echo " --> Installing browserconfig.xml..."
    if test -f "${bconfigInputFile}"; then
        cp "${bconfigInputFile}" "${bconfigOutputFile}"
    else
        echo -e " -->\033[0;31m !! Missing file: ${bconfigInputFile}\033[0m"
    fi
}

addImageIndex() {
    echo
    echo "Adding missing index.html files to image directories..."

    if test -f "${LocalDirectory}${ImageDirectory}index.html"; then
        find "${LocalDirectory}${ImageDirectory}" -type d -exec cp -n "${LocalDirectory}${ImageDirectory}index.html" {} \;
    else
        echo -e " -->\033[0;31m !! Missing file: ${LocalDirectory}${ImageDirectory}index.html\033[0m"
    fi
}

compileLess() {
    local compressFile="$1"

    echo "Compiling {less} into CSS using lessc..."

    for less in ${GothicaLessCSS[@]}; do
        local inputFile="${LocalDirectory}${CSSDirectory}${less}.less"
        local outputFile="${LocalDirectory}${CSSDirectory}${less}.css"
        local outputFileMin="${LocalDirectory}${CSSDirectory}${less}.min.css"

        if test -f "${inputFile}"; then
            # Uncompressed file
            echo " --> Compiling ${less}.less and any imported files..."
            lessc "${inputFile}" "${outputFile}"

            if [ $? -eq 1 ]; then
                exit 1
            fi

            # Compress file
            if [ $compressFile == "true" ]; then
                echo "  --> Minifying CSS.."
                lessc "${inputFile}" "${outputFileMin}" --clean-css="--s1 --advanced --compatibility=ie8"

                if [ $? -eq 1 ]; then
                    exit 1
                fi
            fi
        else
            echo -e " -->\033[0;31m !! Missing file: ${less}.less\033[0m"
        fi
    done
}

compileJS() {
    local compiler="tools/GClosure/compiler.jar"

    echo "Compiling Javascript using Google Closure..."

    for jscript in ${GlobalScripts[@]}; do
        local inputFile="${LocalDirectory}${JavaScriptDir}${jscript}.js"
        local outputFile="${LocalDirectory}${JavaScriptDir}${jscript}.min.js"

        if test -f "${inputFile}"; then
            echo " --> Compiling ${jscript}.js..."
            java -jar "${compiler}" --js="${inputFile}" --js_output_file="${outputFile}"

            if [ $? -eq 1 ]; then
                exit 1
            fi
        else
            echo -e " -->\033[0;31m !! Missing file: ${jscript}.js\033[0m"
        fi
    done
}

fixRemotePermissions() {
    echo "Fixing Permissions..."
    ssh "${1}" -t "su -c'/root/tools/fixperms.sh'"
}

reloadConfig() {
    source $configFile ${1}
}

remoteUpload() {
    local host="${1}"

    if [ ${2} == true ]; then
        # Live host
        local excludes=${ExcludesLive}
    else
        # Dev Host
        local excludes=${ExcludesDev}
    fi

    echo "Updating remote source..."

    echo " --> Copying Files"
    if [ ${EnableExcludes} == true ]; then
        rsync -azve ssh "${LocalDirectory}" "${host}":"${RemoteDirectory}" --delete --exclude-from="${excludes}"
    else
        rsync -azve ssh "${LocalDirectory}" "${host}":"${RemoteDirectory}" --delete
    fi

    if [ ${3} == true ]; then
        echo " --> Updating Vendor"
        rsync -azve ssh "${VendorDirectory}" "${host}":"${RemoteVendorDirectory}" --delete
    fi
}

initConnection() {
    local host="${1}"

    # Setup a controlmaster where possible
    echo "Connecting to remote host..."
    ssh -MNf ${host}
}

endConnection() {
    local host="${1}"
    ssh -O exit ${host}
}

# Interactive Functions
intro() {
    invalidOption() {
        echo
        echo "Invalid selection! Please select another.."
    }

    echo "Welcome To Eternal Goth Toolkit v${version}!"
    echo
    echo "This toolkit was created to aid development and testing of our services by automating various tasks."
    echo
    echo "Written By: ${author}"
    echo "License: ${license}"
    echo
    echo "Select Configuration:"

    until [ "$selection" = "0" ]; do
        echo
        echo "   1) Website Management"
        echo "   2) Help Centre Management"
        echo "   0) Exit"
        echo

        echo -n "Enter selection: "

        read selection

        case $selection in
            1 ) manageWeb ;;
            2 ) manageHelp ;;
            0 ) exit ;;
            * ) invalidOption ;;
        esac
    done
}

manageHelp() {
    # Reload configuration with help centre related configuration
    reloadConfig help

    installHelp() {
        echo
        echo "WARNING: This could be dangerous!"
        read -r -p "Are you sure you want to do this? [y/N] " response

        case "$response" in
            [yY][eE][sS]|[yY])
                echo

                if [ ${4} == true ]; then
                    configFiles 1
                else
                    configFiles 2
                fi

                compileJS
                compileLess ${3}
                initConnection "${1}"
                remoteUpload "${1}" "${2}" false
                echo

                if [ ${AutoFixPermissions} == true ]; then
                    fixRemotePermissions "${1}"
                else
                    echo "Remember to fix permissions!"
                fi

                endConnection
                cleanFiles
            ;;
            *)
                echo
                echo "Not uploaded!"
            ;;
        esac
    }

    invalidOption() {
        echo "Invalid option!"
    }

    echo
    echo "You are now managing the help centre, what would you like to do?"

    until [ "$selection" = "0" ]; do
        echo
        echo "Source Management:"
        echo "   1) Compile Javascript Files"
        echo "   2) Compile CSS Files"
        echo "   3) Minify CSS Files"
        echo
        echo "Installation & Uploads:"
        echo "   10) Install/Upgrade Development Help Centre (Uncompressed)"
        echo "   11) Install/Upgrade Development Help Centre (Compressed)"
        echo "   12) Install/Upgrade Live Help Centre"
        echo
        echo "Maintenance:"
        echo "   20) Fix permissions on development store"
        echo "   21) Fix permissions on live server"
        echo
        echo "   0) Exit"

        echo
        echo -n "Enter selection: "
        read selection

        case $selection in
            # Source
            1 ) compileJS ;;
            2 ) compileLess false ;;
            3 ) compileLess true ;;

            # Install/Upgrade
            10 ) installHelp "${DevelopmentHost}" false false true ;;
            11 ) installHelp "${DevelopmentHost}" false true true ;;
            12 )
                echo
                echo -e "\033[1;31m!! DANGER AHEAD: You have selected to install or upgrade the live system !!\033[0m"
                installHelp "${LiveHost}" true true false
            ;;

            # Maintenance
            20 )
                echo
                read -r -p "Are you sure you want to do this? [y/N] " response

                case "$response" in
                    [yY][eE][sS]|[yY])
                        fixRemotePermissions "${DevelopmentHost}"
                    ;;
                    *)
                        echo
                        echo "Not fixed!"
                    ;;
                esac
            ;;

            21 )
                echo
                read -r -p "Are you sure you want to do this? [y/N] " response

                case "$response" in
                    [yY][eE][sS]|[yY])
                        fixRemotePermissions "${LiveHost}"
                    ;;
                    *)
                        echo
                        echo "Not fixed!"
                    ;;
                esac
            ;;

            # Brexit
            0 ) exit ;;
            * ) invalidOption ;;
        esac
    done
}

manageWeb() {
    # Reload configuration with website related configuration
    reloadConfig web

    installImg() {
        echo
        read -r -p "Are you sure you want to do this? [y/N] " response

        case "$response" in
            [yY][eE][sS]|[yY])
                echo
                echo "Uploading Images..."
                rsync -azve ssh "${LocalDirectory}""${ImageDirectory}" "${1}":"${RemoteDirectory}""${ImageDirectory}" --delete
                fixRemotePermissions "${1}"
            ;;
            *)
                echo
                echo "Not uploaded!"
            ;;
        esac
    }

    installWeb() {
        echo
        echo "WARNING: This could be dangerous!"
        read -r -p "Are you sure you want to do this? [y/N] " response

        case "$response" in
            [yY][eE][sS]|[yY])
                echo

                if [ ${4} == true ]; then
                    configFiles 1
                else
                    configFiles 2
                fi

                compileJS
                compileLess ${3}
                initConnection "${1}"
                remoteUpload "${1}" "${2}" true
                echo

                if [ ${AutoFixPermissions} == true ]; then
                    fixRemotePermissions "${1}"
                else
                    echo "Remember to fix permissions!"
                fi

                endConnection "${1}"
                cleanFiles
            ;;
            *)
                echo
                echo "Not uploaded!"
            ;;
        esac
    }

    invalidOption() {
        echo "Invalid option!"
    }

    echo
    echo "You are now managing the website, what would you like to do?"

    until [ "$selection" = "0" ]; do
        echo
        echo "Source Management:"
        echo "   1) Compile Javascript Files"
        echo "   2) Compile CSS Files"
        echo "   3) Minify CSS Files"
        echo "   4) Add Missing Image Index Files"
        echo
        echo "Installation & Uploads:"
        echo "   10) Upload New Product Images (LIVE)"
        echo "   11) Install/Upgrade Development Store (Uncompressed)"
        echo "   12) Install/Upgrade Development Store (Compressed)"
        echo "   13) Install/Upgrade Live Store"
        echo
        echo "Maintenance:"
        echo "   20) Fix permissions on development store"
        echo "   21) Fix permissions on live server"
        echo
        echo "   0) Exit"

        echo
        echo -n "Enter selection: "
        read selection

        case $selection in
            # Source
            1 ) compileJS ;;
            2 ) compileLess false ;;
            3 ) compileLess true ;;
            4 ) addImageIndex ;;

            # Install/Upgrade
            10 ) installImg "${LiveHost}" ;;
            11 ) installWeb "${DevelopmentHost}" false false true ;;
            12 ) installWeb "${DevelopmentHost}" false true true ;;
            13 )
                echo
                echo -e "\033[1;31m!! DANGER AHEAD: You have selected to install or upgrade the live system !!\033[0m"
                installWeb "${LiveHost}" true true false
            ;;

            # Maintenance
            20 )
                echo
                read -r -p "Are you sure you want to do this? [y/N] " response

                case "$response" in
                    [yY][eE][sS]|[yY])
                        fixRemotePermissions "${DevelopmentHost}"
                    ;;
                    *)
                        echo
                        echo "Not fixed!"
                    ;;
                esac
            ;;

            21 )
                echo
                read -r -p "Are you sure you want to do this? [y/N] " response

                case "$response" in
                    [yY][eE][sS]|[yY])
                        fixRemotePermissions "${LiveHost}"
                    ;;
                    *)
                        echo
                        echo "Not fixed!"
                    ;;
                esac
            ;;

            # Brexit
            0 ) exit ;;
            * ) invalidOption ;;
        esac
    done
}

# Let's get started!
if [ ${configured} == true ]; then
    intro
else
    echo "Not configured!"
fi
